from ipykernel.kernelbase import Kernel
import logging

import ITeradata
import teradata

logger = logging.getLogger('ITeradata')

class TeradataKernel(Kernel):
    implementation = 'ITeradata'
    implementation_version = ITeradata.__version__
    language_info = {'name': 'sql', 'mimetype': 'text/x-sql', 'file_extension': '.sql'}
    banner = 'Teradata kernel'
    
    row_limit = 2000

    def __init__(self, **kwargs):
        Kernel.__init__(self, **kwargs)
        self.td_session = None
        
    
    def connect(self):
        udaExec = teradata.UdaExec()
        self.td_session = udaExec.connect("${dataSourceName}")
    

    def make_msg(self, data):
        return {'data': {'text/html': self.make_html(data), 'text/plain': self.make_plain(data)},
                'metadata': {}}

         
    def make_html(self, data):
        html = '<div><table border="1" cellpadding="2" cellspacing="0" style="border:1px solid black;border-collapse:collapse;">'
        
        for index, entry in enumerate(data):
            if index == 0:
                html += '<tr style="background-color:lightgray">'
                for c in entry: html += '<th><b>' + c + '</b></th>'
            else:
                if index % 2 == 0:
                    html += '<tr style="background-color:ivory">'
                else:
                    html += '<tr style="background-color:aliceblue">'
                    
                for c in entry: html += '<td>' + c + '</td>'
            html += '</tr>'
        
        html += '</table></div>'
        
        return html    
    

    def make_plain(self, data):
        txt = ''
        for d in data: txt += str(d) + '\n'
        return txt
    
    
    def exec_query(self, sql):
        try:
            # lazy connection
            if self.td_session is None: self.connect()
        
            result = [] 
            cursor = self.td_session.execute(sql)
            
            if cursor.description is not None:
                header = [d[0] for d in cursor.description]
                result.append(header)
            
            count = 0
            while count < TeradataKernel.row_limit:
                try:
                    row = cursor.next()
                    result.append([str(v) for v in row.values])
                    count += 1
                except StopIteration:
                    break
            return result
        except Exception as e:
            raise e

    
    def cleanup(self):
        if self.td_session is not None:
            cursor = self.td_session.cursor()
            if cursor is not None: cursor.close()
            self.td_session.close()
            
            
    def do_execute(self, code, silent, store_history=True,
        user_expressions=None, allow_stdin=False):

        is_error = 0
        if not silent:
            try:
                result = self.exec_query(code)
                if len(result) > 0:
                    msg = self.make_msg(result)
                    self.send_response(self.iopub_socket, 'display_data', msg)
            except Exception as e:
                logger.error(e)
                is_error = 1
                logger.error('[' + str(self.execution_count) + ']' + ' ' + e.msg)
                self.send_response(self.iopub_socket, 'stream', {'name': 'stderr', 'text': e.msg})
        
        response = {'status': 'ok' if is_error == 0 else 'error',
                'execution_count': self.execution_count,
                'payload': [],
                'user_expressions': {},
               }
        
        if is_error == 1:
            response['ename'] = 'ITeradata ERROR'
            response['evalue'] = 1
        
        return response
    
    
    def do_shutdown(self, restart):
        self.cleanup()
        return Kernel.do_shutdown(self, restart)
