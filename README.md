# ITeradata#
Jupyter kernel for teradata

## Prerequisions##
1. Install and config [Teradata python module](https://developer.teradata.com/tools/reference/teradata-python-module)
2. Put `udaexec.ini` under `/etc`

## Installation##

        jupyter kernelspec install --replace --user teradata
        python setup.py install

## Todo##
1. ~~Better error handling~~
2. Auto completion
3. IPython magic commands for query options
