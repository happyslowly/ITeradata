import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="ITeradata",
    version="0.0.1",
    author="Xin Xu",
    author_email="happyslowly@gmail.com",
    description=("A jpuyter kernel for teradata"),
    packages=['ITeradata'],
    long_description=read('README.md'),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Topic :: Database',
        'Topic :: Database :: Front-Ends',
        ]
)
